# Lab: Hello

Hello!

Your task is to implement the `hello` function:

@include src/main/scala/hello/hello.scala hello scaffold

## Solution

Here is the solution:

@include src/main/scala/hello/hello.scala hello solution

Make sure you understand everything. If you have any questions, do not hesitate to ask the TAs or the instructors, during the lab session or on Ed.
