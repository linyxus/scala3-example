package hello

class HelloTest extends munit.FunSuite:
  test("works with Ada"):
    assertEquals(hello("Ada"), "Hello, Ada!")

  test("works with Bob"):
    assertEquals(hello("Bob"), "Hello, Bob!")
