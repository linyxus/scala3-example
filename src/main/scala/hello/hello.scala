package hello

// start hello
def hello(name: String) =
//---
  f"Hello, $name!"
/*+++
  "Hello, World!"
+++*/
// end hello
