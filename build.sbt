name := "hello"
scalaVersion := "3.3.1"

libraryDependencies += "org.scala-lang" %% "toolkit" % "0.1.7"
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
libraryDependencies += "com.lihaoyi" %% "os-lib" % "0.9.1"
libraryDependencies += "com.lihaoyi" %% "upickle" % "3.1.3"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.17.0"
libraryDependencies += "com.softwaremill.sttp.client3" %% "core" % "3.9.1"
libraryDependencies += "ch.epfl.lara" %% "lisa" % "0.6" from "https://github.com/sankalpgambhir/lisa/raw/cs214/assembly/target/scala-3.3.1/lisa-assembly-0.6.jar"
libraryDependencies += "com.lihaoyi" %% "sourcecode" % "0.3.1"

libraryDependencies += "org.scalameta" %% "munit" % "1.0.0-M10" % Test
libraryDependencies += "org.scala-lang" %% "toolkit-test" % "0.1.7" % Test
libraryDependencies += "org.scalatestplus" %% "scalacheck-1-17" % "3.2.17.0" % "test"
libraryDependencies += "io.github.martinhh" %% "scalacheck-derived" % "0.4.2" % Test
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.17.0" % Test

scalacOptions ++= Seq("-source:future", "-deprecation", "-language:fewerBraces", "-Xfatal-warnings")
